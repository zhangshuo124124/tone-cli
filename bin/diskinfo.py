#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
from disksetup import diskinfo


def add_subcommand(subparser, subcommand, help=""):
    local_parser = subparser.add_parser(subcommand, help=help)
    local_parser.set_defaults(subcommand=subcommand)
    return local_parser


def output_device(devs):
    print(" ".join([os.path.join('/dev', d) for d in devs]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(help="Get disk information")
    # data_partitio
    sub_command = add_subcommand(
        subparser,
        'data_partition',
        help="Get Data partitions")
    sub_command.add_argument(
        '-n',
        action='store_true',
        help="Show Data partitions number")
    # data_disk
    sub_command = add_subcommand(subparser, 'data_disk', "Get Data disks")
    sub_command.add_argument(
        '--memory',
        help="Memory disk type eg: null brd scsi peme"
    )
    # data_mountpoints
    sub_command = add_subcommand(
        subparser,
        'data_mountpoints',
        help="Get Data disks mountpoints")
    sub_command.add_argument(
        '--nfs',
        action='store_true',
        help="nfs disk mounted points under /nfs"
    )

    config = parser.parse_args()

    if config.subcommand == 'data_partition':
        parts = diskinfo.get_test_parts()
        lvms = diskinfo.get_lvm_parts()
        if lvms:
            parts = diskinfo.get_test_parts()

        if config.n:
            print(len(parts))
        else:
            output_device(parts)
    elif config.subcommand == 'data_disk':
        disks = diskinfo.get_test_disks()
        output_device(disks)
    elif config.subcommand == 'data_mountpoints':
        mps = diskinfo.get_test_mountpoints()
        print(" ".join(mps))
