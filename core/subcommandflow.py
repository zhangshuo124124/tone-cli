#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utils.log import logger
from dispatcher import SubcommandDispatcher, SubcommandPreDispatcher


class SubcommandFlow(object):
    def __init__(self, testconf):
        logger.debug("Init ......")
        self.config = testconf

    def __enter__(self):
        logger.debug("Enter ......")
        dispatcher = SubcommandPreDispatcher()
        dispatcher.map_method('run', self)
        return self

    def __exit__(self, _exc_type, _exc_value, _traceback):
        logger.debug("Exit ......")

    def run(self):
        logger.debug("Run ......")
        dispatcher = SubcommandDispatcher()
        dispatcher.map_method('run', self)
