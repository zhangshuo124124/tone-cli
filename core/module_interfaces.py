#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Module(object):
    priority = 1000
    pass


class CLI(Module):
    def configure(self, parser):
        raise NotImplementedError

    def run(self, config):
        raise NotImplementedError


class JOBPRE(Module):
    def pre(self, job_instance):
        raise NotImplementedError


class JOBPOST(Module):
    def post(self, job_instance):
        raise NotImplementedError


class TESTMODULE(Module):
    def run(self, test_instance):
        raise NotImplementedError


class DEPLOY(Module):
    def run(self, uri, config):
        raise NotImplementedError

    def restore(self):
        raise NotImplementedError


class PARAM(Module):
    _instance = None
    value = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        super(PARAM, self).__init__()

    def get(self, value):
        raise NotImplementedError

    def generate(self):
        return None
