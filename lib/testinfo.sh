#!/bin/sh

[ -n "$lib_testinfo_loaded" ] && return
lib_testinfo_loaded=1

add_custom_testinfo()
{
	local key=$1
	shift
	local str="$@"
	local dir="$TONE_CURRENT_RESULT_DIR/testinfo"

	[ -d "$dir" ] || mkdir -p $dir || return
	echo "$str" >> $dir/$key
}

add_testinfo_cpu_model()
{
	local model_name=$(cat /proc/cpuinfo | grep "^model name" | \
		tail -n 1 | awk -F: '{print $2}' | sed 's/^\ *//g')
        if [ -n "$model_name" ]; then
		case $model_name in
		"Intel(R) Xeon(R)"*)
			model_name_strip=Intel-Xeon-$(echo "$model_name" | awk '{print $4}')
			;;
		"AMD EPYC"*)
			model_name_strip=$(echo "$model_name" | awk '{print $1"-"$2"-"$3}')
			;;
		"Hygon C86"*)
			model_name_strip=$(echo "$model_name" | awk '{print $1"-"$2"-"$3}')
			;;
		*)
			model_name_strip=$(echo "$model_name" | sed -e 's/(R)//g' -e 's/@//g' -e 's/ /-/g')
			;;
                esac
                add_custom_testinfo cpu_model $model_name_strip
                return
        fi

	# if no model name in /proc/cpuinfo
	which dmidecode &>/dev/null || yum -y install dmidecode
	dmidecode --help &>/dev/null && {
		local cpu_manufacturer=$(dmidecode -t processor | \
			grep Manufacturer | tail -n 1 | \
			awk -F: '{print $2}' | xargs | \
			sed 's/ /-/g')
		local cpu_version=$(dmidecode -t processor | \
			grep Version | tail -n 1 | \
			awk -F: '{print $2}' | awk -F/ '{print $1}' | \
			xargs | sed -e 's/+//g' -e 's/ /-/g')
	}
	[ -n "$cpu_manufacturer" ] && [ "$cpu_manufacturer" != "Alibaba-Cloud" ] && {
		add_custom_testinfo cpu_model ${cpu_manufacturer}-${cpu_version}
		return
	}

	# try to guess processor of arm vm
	[ "$(uname -m)" = "aarch64" ] && {
		local cpu_implementer=$(cat /proc/cpuinfo | grep "^CPU implementer" | \
			tail -n 1 | awk -F: '{print $2}' | sed 's/^\ *//g')
		local cpu_partid=$(cat /proc/cpuinfo | grep "^CPU part" | \
			tail -n 1 | awk -F: '{print $2}' | sed 's/^\ *//g')
		# vendor_id: 0x48 -- HiSilicon
		# part_id: 0xd01 -- Kunpeng-920
		if [ "$cpu_implementer" = "0x48" ]; then
			if [ "$cpu_partid" = "0xd01" ]; then
				model_name="Kunpeng-920"
			else
				model_name="ARM64-XXX"
			fi
			add_custom_testinfo cpu_model HiSilicon-$model_name
			return
		fi
		# vendor_id: 0x70 -- PHYTIUM
		# part_id: 0x662 -- FT2000PLUS
		if [ "$cpu_implementer" = "0x70" ]; then
			if [ "$cpu_partid" = "0x662" ]; then
				model_name="FT2000PLUS"
			else
				model_name="ARM64-XXX"
			fi
			add_custom_testinfo cpu_model PHYTIUM-$model_name
			return
		fi
	}

	# for Alibaba Cloud ECS
	[ -n "$cpu_manufacturer" ] && [ "$cpu_manufacturer" = "Alibaba-Cloud" ] && {
		add_custom_testinfo cpu_model Alibaba-Cloud-ECS-$(uname -m)
		return
	}

	# otherwise
	add_custom_testinfo cpu_model $(uname -m) Unknown
}

add_testinfo_pkginfo()
{
	local pkg=$1
	local tempfile=$(mktemp)

	yum info $pkg > $tempfile || { rm -f $tempfile; return 1; }
	pkg_name=$(grep ^Name $tempfile | awk '{print $NF}')
	pkg_arch=$(grep ^Arch $tempfile | awk '{print $NF}')
	pkg_ver=$(grep ^Version $tempfile | awk '{print $NF}')
	pkg_rel=$(grep ^Release $tempfile | awk '{print $NF}')
	pkg_repo=$(grep -E "^Repo|^From" $tempfile | awk '{print $NF}' | \
		tail -n 1 | sed 's/@//g')
	rm -f $tempfile
	[ -n "$pkg_name" ] || return 1
	add_custom_testinfo packages $pkg_name ${pkg_ver}-${pkg_rel} $pkg_arch $pkg_repo
}
