#accelerator-test 

## Description
accelerator测试.

## Homepage
[https://codeup.aliyun.com/5f4e0dfe6207a1a8b17fa7cf/kangaroo-test/accelerator-test.git]


## Version
master

## Category
functional

## Parameters

## Results

## Manual Run
```
1. 在测试脚本中使用全局变量 PKG_CI_ABS_RPM_URL 来引用被测试的 RPM 包。
   环境变量 PKG_CI_ABS_RPM_URL 会在运行测试之前被设置，它包含了被测 RPM 包的 URL，例如
   PKG_CI_ABS_RPM_URL="https://abs.openanolis.cn/download/OpenAnolis/opa-fm-10.12.1.0.6-1.0.1.an8.src.rpm,https://abs.openanolis.cn/download/OpenAnolis/opa-fm/opa-fm-10.12.1.0.6-1.0.1.an8.x86_64.rpm"

2. 执行以下命令运行所有测试用例:
   ./runtest.sh -d ./tests

3. 执行以下命令运行指定测试用例:
   ./runtest.sh -d ./tests -t example1_test.sh -t example2_test.sh

4. 执行以下命令运行指定标签的测试用例:
   ./runtest.sh -d ./tests -o tag1 

```
