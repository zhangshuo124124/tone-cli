#!/bin/bash

get_kernel_info()
{
    kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
    os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')
    arch=$(uname -m)
}

skip_Non_executable_case()
{
    local case=$1
    ker_ver=$(uname -r | cut -d. -f1)
    if [ $ker_ver -le 4 ]; then
        local show_name=$(echo $case | sed 's|:|.|g')
        local tmp_case=$(echo $case | sed 's|:|: |g')
        cat $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh | grep "$tmp_case" && echo "ignored_by_tone $show_name: Skip"
        sed -i "/${tmp_case}$/d" $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh
        case=$(echo $case |awk -F":" '{print $NF}')
        sed -i "/${case}\ /d" $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh
    else
        local show_name=$(echo $case | sed 's|:|.|g')
        cat $TONE_BM_RUN_DIR/kselftest-list.txt | grep "^$case$" && echo "ignored_by_tone $show_name: Skip"
        sed -i "\|^${case}$|d" $TONE_BM_RUN_DIR/kselftest-list.txt
    fi
}

prepare_for_blacklist()
{
    grep -q sgx /proc/cpuinfo || grep -q "^sgx:test_sgx" $blacklist || echo "sgx:test_sgx" >>$blacklist
    # amx_64 testcase can only run on SPR machine
    local model_value=$(lscpu | grep 'Model:' | awk -F ':' '{print $2}' | tr -d ' ')
    [ x"$model_value" == x"143" ] || grep -q "^x86:amx_64" $blacklist || echo "x86:amx_64" >>$blacklist
}

skip_case()
{
    blacklist="$TONE_BM_SUITE_DIR/blacklist/${os_ver}_${kernel_ver}_${arch}_blacklist"
    config_blacklist="$TONE_BM_SUITE_DIR/config_blacklist"
    prepare_for_blacklist
    [ ! -f $blacklist ] && [ ! -f $config_blacklist ] && echo "not exist $blacklist and $config_blacklist" && return  
    # 删除blacklist中的用例
    while read line; do
	    if echo $line |grep -q ^'#';then
	        continue
	    fi
        skip_Non_executable_case "$line"
    done < $TONE_BM_SUITE_DIR/blacklist/${os_ver}_${kernel_ver}_${arch}_blacklist
    # 删除未开启config选项的用例
    while read line; do
        local config=$(echo $line |awk '{print $1}')
        local case_name=$(echo $line |awk '{print $2}')
        cat /boot/config-$(uname -r) |grep -q "$config=" || skip_Non_executable_case "$case_name"
    done < $TONE_BM_SUITE_DIR/config_blacklist 
}

setup()
{
    local k_basepath=$TONE_BM_RUN_DIR
    uname -r | grep -q 4.19 && k_basepath=$TONE_BM_RUN_DIR/kselftest

    get_kernel_info
    skip_case

    # prepare for bpf
    [ -d $k_basepath/bpf ] && echo "timeout=100" > $k_basepath/bpf/settings

    # prepare for net
    [ -d $k_basepath/net ] && echo "timeout=100" > $k_basepath/net/settings

    # prepare for seccomp
    [ -d $k_basepath/seccomp ] && echo "timeout=100" > $k_basepath/seccomp/settings

    # prepare for epoll
    [ -d $k_basepath/filesystems/epoll ] && echo "timeout=100" > $k_basepath/filesystems/epoll/settings
}

run ()
{
    # https://bugzilla.openanolis.cn/show_bug.cgi?id=1139
    maxlockmem_value=$(ulimit -l)
    ulimit -l unlimited

    # run kernel_selftests
    cd $TONE_BM_RUN_DIR/kselftest/
    [ -f ./run_kselftest.sh ] && ./run_kselftest.sh || ../run_kselftest.sh

    # restore  max locked memory value
    ulimit -l ${maxlockmem_value}
}

parse ()
{
    $TONE_BM_SUITE_DIR/parse.py
}
