#!/bin/bash

pass()
{
	echo "====PASS: $*"
}

fail()
{
	echo "====FAIL: $*"
}

warn()
{
	echo "====WARN: $*"
}

skip()
{
	echo "====SKIP: $*"
}

prepare_build_repo()
{
	[ "$KERNEL_CI_REPO_BRANCH" == "devel-6.6" ] && {
		yum install -y asciidoc || {
			cat > /etc/yum.repos.d/build.repo <<EOF
[build]
name=build
baseurl=http://8.131.87.1/kojifiles/repos/dist-an23-build/latest/$(uname -m)/
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ANOLIS
gpgcheck=0
EOF
		}
	}
}

anck_build()
{
	prepare_build_repo
	build_script=$TONE_BM_SUITE_DIR/anck_build.sh
	python $TONE_BM_SUITE_DIR/anck_build.py $build_script
	upload_archives $(find /anck_build/ck-build/outputs -name *.rpm)
	return 0
}

kabi_install_dep_pkg()
{
	dep_pkgs="elfutils elfutils-devel bison flex"
	for pkg in $dep_pkgs; do
		remote_cmd "yum install -y $pkg"
	done 
}

check_kabi()
{
	local reboot_kernel_ret=$1
	if [ "$reboot_kernel_ret" != "0" ]; then
		skip "check_kabi"
		return 1
	fi

	if [ "$KERNEL_CI_REPO_BRANCH" != "devel-5.10" ]; then
		skip "check_kabi"
		return 0
	fi

	kabi_test_dir="/tmp/kabi_test"
	kabi_test_log="/tmp/kabi_test.log"

	kabi_install_dep_pkg

	rm -rf $kabi_test_log
	remote_cmd "[ -d $kabi_test_dir ] && rm -rf $kabi_test_dir"
	remote_cmd "mkdir -p $kabi_test_dir"
	remote_cmd "git clone https://gitee.com/anolis/kabi-dw.git $kabi_test_dir/kabi-dw"
	remote_cmd "\"cd $kabi_test_dir/kabi-dw;make\""
	remote_cmd "git clone https://gitee.com/anolis/kabi-whitelist.git $kabi_test_dir/kabi-whitelist"

	vmlinux_new=$(find /anck_build/ck-build/rpmbuild/BUILD/ -name "vmlinux" | grep -v compressed)
	[ -z "$vmlinux_new" ] && echo "Can not found kernel vmlinux" && return 1

	# copy vmlinux to remote
	vmlinuz_path=$(remote_cmd "find /lib/modules -name 'vmlinuz'" | tail -1)
	vmlinuz_dir=$(dirname $vmlinuz_path)
	logger "scp ${vmlinux_new} root@${REMOTE_HOST}:${vmlinuz_dir}"

	# kabi compare
	remote_cmd "$kabi_test_dir/kabi-dw/kabi-dw generate -s ${kabi_test_dir}/kabi-whitelist/kabi_whitelist_$(uname -m) -o kabi_after_$(uname -m) ${vmlinuz_dir}"
	remote_cmd "$kabi_test_dir/kabi-dw/kabi-dw compare -k ${kabi_test_dir}/kabi-whitelist/kabi_dw_output/kabi_pre_$(uname -m) kabi_after_$(uname -m) > $kabi_test_log"

	ls -l $kabi_test_log || {
		echo "Can not found $kabi_test_log"
		fail "check_kabi"
		return 1
	}

	diff_line=$(cat $kabi_test_log | wc -l)
	[ "$diff_line" -gt 0 ] && {
		echo "========show kabi compare result ========="
		cat $kabi_test_log
		upload_archives $kabi_test_log
		fail "check_kabi"
		return 1
	}
	pass "check_kabi"
	return 0
}

anck_boot_test()
{
	local rpm_build_ret=$1
        if [ "$rpm_build_ret" != "0" ]; then
                skip "boot_kernel_rpm"
                return 1
        fi

	[ -n "$REMOTE_HOST" ] || {
		echo "Error: REMOTE_HOST is not set"
		fail "boot_kernel_rpm"
		return 1
	}

	anck_rpms_dir="/anck_build/ck-build/outputs/0"
	kver_new=$(find $anck_rpms_dir -name kernel-headers*.rpm | \
		head -n 1 | xargs \
		rpm -qp --queryformat="%{VERSION}-%{RELEASE}.%{ARCH}\n")

	remote_check || return 1
	remote_cmd "rm -rf /anck_rpms"
	logger "scp -r $anck_rpms_dir root@$REMOTE_HOST:/anck_rpms"
	remote_cmd "rpm -Uvh --force /anck_rpms/*.rpm"
	remote_cmd "reboot"
	sleep 30

	[ -n "$REBOOT_TIMEOUT" ] || REBOOT_TIMEOUT=600
	wait_time=30
	ret=1
	while [ "$wait_time" -lt "$REBOOT_TIMEOUT" ]
	do
		remote_check | grep -q $kver_new && ret=0 && break
		echo "Still waiting boot..."
		wait_time=$((wait_time + 30))
		sleep 30
	done
	remote_check
	if [ "$ret" -ne 0 ]; then
		echo "Error: failed to boot $kver_new in ${REBOOT_TIMEOUT}s"
		fail "boot_kernel_rpm"
	else
		echo "Succeed to boot new kernel!"
		pass "boot_kernel_rpm"
	fi
	return $ret
}

remote_check()
{
	logger "ping -c 1 $REMOTE_HOST" || return 1
	logger "timeout 5 ssh root@$REMOTE_HOST uname -r" || return 1
	return 0
}

check_dmesg()
{
	local dmesg_info=$(remote_cmd "dmesg -l err -T | grep -v '\/etc\/keys\/x509_'" | grep -v dmesg)
	if ! [ -z "${dmesg_info}" ]; then
		echo "Got err from dmesg"
		return 1
	else
		echo "Check dmesg success."
		return 0
	fi
}

remote_cmd()
{
	logger "ssh root@$REMOTE_HOST $*"
}


show_result()
{
	local case_name=$1
	local m_ret=$2

	if [ $m_ret -ne 0 ]; then
		if [ "$case_name" == "check_Kconfig" ]; then
			warn $case_name
		else
			fail $case_name
		fi
	else
		pass $case_name
	fi
}

run()
{
	run_ret=0
	anck_build_dir="/anck_build"
	[ -d "$anck_build_dir" ] || mkdir -p $anck_build_dir
	# remove anck build log before build
	rm -f /tmp/anck_*.log
	# build ANCK
	anck_build

	# check cases result
	caselist="check_Kconfig build_allyes_config build_allno_config build_anolis_defconfig build_anolis_debug_defconfig anck_rpm_build"
	for case in $caselist; do
		# archive build logs
		upload_archives /tmp/anck_${case}.log

		tail -n 5 /tmp/anck_${case}.log | grep -q "$case: pass"
		c_ret=$?
		show_result $case $c_ret

		if [ "$case" == "anck_rpm_build" ]; then
			anck_boot_test $c_ret
			check_kabi $?
			c_ret=$?
		fi
		[ "$case" != "check_Kconfig" ] && [ "$c_ret" != "0" ] && run_ret=1
	done
	return $run_ret
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
