#!/bin/bash

run()
{
	[ -n "$nr_threads" ] && logger export OMP_NUM_THREADS=$nr_threads
	[ -n "$omp_places" ] && logger export OMP_PLACES=$omp_places
	[ -n "$omp_bind" ] && logger export OMP_PROC_BIND=$omp_bind
	cd "$TONE_BM_RUN_DIR" || exit
	logger $NUMACTL ./stream_c.exe

}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}
