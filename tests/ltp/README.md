# ltp

## Description

The LTP testsuite contains a collection of tools for testing the Linux kernel and related features.

## Homepage
[https://gitee.com/anolis/ltp.git](https://gitee.com/anolis/ltp.git)

Mirrored from upstream LTP project:
[https://github.com/linux-test-project/ltp](https://github.com/linux-test-project/ltp)

## Version
master

## Category
functional

## Parameters

## Results

## Manual Run
```
1, Run all tests
./runltp

2, Run group tests
./runltp -f ipc

3, Run single test
./runltp -f ipc -s pipeio_1
```
