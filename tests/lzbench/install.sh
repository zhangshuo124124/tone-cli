WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/lzbench-1.8.1.tar"
STRIP_LEVEL=1
if [[ "ubuntu debian uos kylin" =~ $TONE_OS_DISTRO ]]; then
    DEP_PKG_LIST="build-essential"
else
    DEP_PKG_LIST="gcc-c++"
fi

build()
{
	make -j
}

install()
{
	cp -af * $TONE_BM_RUN_DIR/
}

