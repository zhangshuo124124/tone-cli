#!/bin/bash
MODULE_NAME=tdx-compliance

setup()
{
    if [[ "$feature" == "tdx-compliance" ]]; then
        if ! lsmod | grep -q "tdx_compliance"; then
            modprobe $MODULE_NAME ||
            insmod $TONE_BM_CACHE_DIR/tdx-compliance/tdx-compliance.ko
        fi
    fi
}

check_vm()
{
    out=`LANG=C lscpu |grep -P 'Hypervisor vendor:.*KVM'`
    if [[ "$out" != "" ]]; then
        echo yes
    else
        echo no
    fi
}

# This function is pending on kernel interface
check_tdvm_version()
{
    echo todo
}

run()
{
    if [ ! -d "$TONE_BM_CACHE_DIR" ]; then
        echo "Error: Please run the command tone install intel-lkvs first."
        exit 1
    fi

    # Check each lines in conf/intel-lkvs
    if [[ "$feature" == "tdx-compliance" ]]; then
        if [[ `check_vm` == "yes" ]]; then
            echo all 1.0 > /sys/kernel/debug/tdx/tdx-tests
            cat /sys/kernel/debug/tdx/tdx-tests
        else
            echo "tdx-compliance SKIP"
        fi
    elif [[ "$feature" == "xsave" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f xsave/tests -o ./results_xsave.log
#        $TONE_BM_CACHE_DIR/xsave/xstate_64
    elif [[ "$feature" == "pt" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f pt/tests -o ./results_pt.log
    elif [[ "$feature" == "splitlock" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f splitlock/tests -o ./results_splitlock.log
    elif [[ "$feature" == "umip" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f umip/tests -o ./results_umip.log
    elif [[ "$feature" == "ifs" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f ifs/tests -o ./results_ifs.log
    elif [[ "$feature" == "isst" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f isst/tests -o ./results_isst.log
    elif [[ "$feature" == "pmu" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f pmu/tests -o ./results_pmu.log
    elif [[ "$feature" == "sdsi" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f sdsi/tests -o ./results_sdsi.log
    elif [[ "$feature" == "ufs" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f ufs/tests -o ./results_ufs.log
    elif [[ "$feature" == "cstate" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f cstate/tests-server -o ./results_cstate.log
    elif [[ "$feature" == "rapl" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f rapl/tests-server -o ./results_rapl.log
    elif [[ "$feature" == "topology" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f topology/tests-server -o ./results_topology.log
    elif [[ "$feature" == "thermal" ]]; then
        cd $TONE_BM_CACHE_DIR
        ./runtests -f thermal/thermal-tests -o ./results_thermal.log
    fi
}

teardown()
{
    #find tdx-compliance module, rmmod it
    if [[ "$feature" == "tdx-compliance" ]]; then
        if lsmod | grep -q "tdx_compliance"; then
            modprobe -r $MODULE_NAME
        fi
    fi
    echo done
}

parse()
{
    awk '
	match($0, /^[0-9]+:\s*([^:]+):\s*(PASS|FAIL)/, arr) {
	    print arr[1] ":" arr[2]
        }
	match($0, /^[0-9]+: ([^[]+): \[(PASS|FAIL)\]/, arr) {
	    print arr[1] ":" arr[2]
	}
	'
}
