GIT_URL="https://github.com/intel/lkvs.git"
BRANCH="main"

fetch()
{
    git_clone $GIT_URL $TONE_BM_CACHE_DIR
}

build_component() {
    local component=$1
    if [ ! -d "${TONE_BM_CACHE_DIR}/${component}" ]; then
        fetch
    fi
    cd "${TONE_BM_CACHE_DIR}/${component}" || exit 1
    make
}

build() {
    build_component "xsave"
    build_component "pt"
    build_component "splitlock"
    build_component "umip"
    build_component "tdx-compliance"
}

install()
{
    if lsmod | grep -q "tdx_compliance"; then
	$(rmmod tdx_compliance)
    fi

    insmod tdx-compliance.ko

    echo "success in insmod tdx-compliance.ko"
}
