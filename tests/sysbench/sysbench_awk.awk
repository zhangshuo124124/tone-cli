#!/usr/bin/awk -f
  


BEGIN { FS = "[: )\t/(]+" }

/read:/ { printf "queries_performed_read: %.2f ms\n", $NF }
/write:/ { printf "queries_performed_write: %.2f ms\n", $NF }
/other:/ { printf "queries_performed_other: %.2f ms\n", $NF }
/total:/ { printf "queries_performed_total: %.2f ms\n", $NF }
/transactions:/ {
    printf "queries_performed_transactions: %.2f ms\n", $(NF-4), $NF
    printf "queries_performed_transactions_per_sec: %.2f\n", $(NF-3), $NF
}
/queries:/ {
    printf "queries_performed_queries: %.2f ms\n", $(NF-4), $NF
    printf "queries_performed_queries_per_sec: %.2f\n", $(NF-3), $NF
}
/ignored errors:/ {
    printf "queries_performed_ignored_errors: %.2f ms\n", $(NF-4), $NF
    printf "queries_performed_ignored_errors_per_sec: %.2f\n", $(NF-3), $NF
}
/reconnects:/ {
    printf "queries_performed_reconnects: %.2f ms\n",$(NF-4), $NF
    printf "queries_performed_reconnects_per_sec: %.2f\n", $(NF-3), $NF
}
BEGIN { FS = "[: )\t/(]+" }

/transferred \(.* MiB\/sec\)/ { printf "Throughput_MB: %.2f MB/s\n", $(NF-3) }
/total time/ { total_time = $NF }
/time elapsed/ { total_time = $NF }
/total number of events:/ { printf "workload: %.2f\n", $NF; events = $NF }
/events per second:/ { printf "Throughput_eps: %.2f \n", $NF; Throughput_eps = $NF }
/min:/ { printf "latency_min: %.2f ms\n", $NF }
/avg:/ { printf "latency_avg: %.2f ms\n", $NF }
/max:/ { printf "latency_max: %.2f ms\n", $NF }
/95th percentile:/ { printf "latency_95th: %.2f ms\n", $NF }
/sum:/ { printf "latency_sum: %.2f ms\n", $NF }
/events \(avg\/stddev\):/ {
    printf "thread_events_avg: %.2f\nthread_events_stddev: %.2f\n", $(NF-1), $NF
}
/execution time \(avg\/stddev\):/ {
    printf "exec_time_avg: %.4f s\nexec_time_stddev: %.2f\n", $(NF-1), $NF
}

END {
    if (Throughput_eps == 0)
        printf "Throughput_eps: %.2f \n", events / total_time
}

