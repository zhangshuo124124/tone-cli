#!/bin/bash

# 使用 ifconfig 命令获取当前系统的 IP 地址
ip_address=$(ifconfig | grep 'inet ' | awk '{print $2}' | grep -v '127.0.0.1' | head -n 1)

# 检查是否成功获取到 IP 地址
if [ -n "$ip_address" ]; then
    echo "当前系统的 IP 地址是: $ip_address"
else
    echo "无法获取系统的 IP 地址"
fi

