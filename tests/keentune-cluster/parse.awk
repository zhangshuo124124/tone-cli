#!/usr/bin/awk -f
BEGIN{
    print "\n";
}

/^test_.*(.*)/ {
    casename = $1
    split($2,m,")")
    j = split(m[1],i,".")
    casedir = i[j]
    result = $NF
    if ( $4 == "skipped" ) {
        testcase_result = "Skip"
    } else if ( result == "ok" ) {
        testcase_result = "Pass"
    } else if ( result == "FAIL" ) {
        testcase_result = "Fail"
    } else {
        testcase_result = "Fail"
    }
    printf("%s-%s: %s\n", casedir, casename, testcase_result)
}
