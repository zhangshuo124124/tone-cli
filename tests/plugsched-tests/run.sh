#!/bin/bash
# - bundle: ci

merge_pull_request()
{
	[ -n "$PKG_CI_PR_URL" ] || return 0
	echo "PKG_CI_PR_URL: $PKG_CI_PR_URL"
	patch_url="${PKG_CI_PR_URL}.patch"
	patch_name=$(basename $patch_url)
	logger "timeout 300 wget $patch_url -O /tmp/$patch_name"
	logger "pwd"
	logger "git status"
	git config user.email "test@example.com"
	git config user.name "test"
	git status | grep -q "ahead of" && git reset --hard origin/HEAD
	logger "git show"
	logger "ls"
	logger "git am /tmp/$patch_name" || return 1
	return 0
}

setup()
{
	arch=$(uname -i)
	if [ "${arch}" = "x86_64" ]; then
		tag="latest"
	else
		tag="latest-${arch}"
	fi
	logger yum -y install podman
	export RETRY_DELAY=10
	retry 3 podman pull "docker.io/plugsched/plugsched-sdk:${tag}"
	return 0
}

run()
{
	logger cd plugsched || return 1
	merge_pull_request || return 1
	logger ./tests/run_test ${bundle:-"ci"}
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
