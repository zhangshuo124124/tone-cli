# fio
## Description
Fio spawns a number of threads or processes doing a particular type of I/O
action as specified by the user. fio takes a number of global parameters, each
inherited by the thread unless otherwise parameters given to them overriding
that setting is given.  The typical use of fio is to write a job file matching
the I/O load one wants to simulate.

## Homepage
https://github.com/axboe/fio

## Version
fio-3.19

## Category
performance

## Parameters
- ioengine: fio 命令使用的io engine
- disk_type: disk自动监测使用测试磁盘
- cpupin: 使用cpu亲和性设置
- fs: 文件系统格式
- test_sieze: 测试处理的size大小
- rw: fio测试中read write 模式顺序读写随机读写的组合
- bs: block size
- iodepth: iodepth设置
- nr_task: fio运行使用的线程数目

## Results
```
latency_750us: 0.01 %
latency_50us: 32.639474 %
latency_100us: 67.251473 %
latency_250us: 0.100316 %
latency_500us: 0.01 %
latency_1000us: 0.01 %
latency_10ms: 0.01 %
latency_2ms: 0.01 %
latency_4ms: 0.01 %
write_bw: 70.93359375 MBps
write_iops: 18159.229469
write_clat_mean: 52715.313887 ns
write_clat_stddev: 10348.982437
write_clat_90%: 59648 ns
write_clat_95%: 61184 ns
write_clat_99%: 71168 ns
workload: 5447787
```

## Manual Run

请参考官方文档
