#!/bin/bash
# - testcase
# - nr_task
# - systemcall

run() {
   
    # 根据 $TONE_BM_RUN_DIR/bin 目录中的文件数量来判断操作系统类型：
    if [ $(ls "$TONE_BM_RUN_DIR"/bin | wc -l) == 1 ];then
     # 如果只有一个文件，则将该文件名赋值给变量 OS，表示操作系统类型；
        OS=`ls "$TONE_BM_RUN_DIR"/bin`
	bindir="./bin/$OS"
    else
    # 否则，将操作系统类型设置为空
        OS=''	
        bindir="./bin"
    fi
    # 将执行路径添加到 PATH 环境变量中
    export PATH=$TONE_BM_RUN_DIR/bin/$OS:$PATH

     从 /proc/meminfo 中提取系统内存总量，并将其转换为以 MB 为单位的值
    TMP=`grep 'MemTotal:' /proc/meminfo | awk '{print $2}'`
    echo TOTAL_MEM=`echo $TMP / 1024 | bc 2>/dev/null`  
    根据 $memory 的大小计算需要使用的内存量（单位：MB），并尝试减少测试时间
    MB=$(( $(size_cn_bytes $memory) / 2 / 1024 / 1024 ))
    #reduce test time
    [[ $MB -ge 1024 ]] && MB=1024
    echo "use memory ${MB}MB"
    # 同时转换为KB
    # 已有变量MB存储了MB值
    KB=$((MB * 1024))
    [[ $KB -ge 1024 ]] && KB=1024
    echo "Use memory: ${KB}KB"
    # 将 nr_task 的值乘以 100等于测试的持续时间
    w=$((nr_task * 100))
    #将 nr_task 的值乘以 10等于测试的次数
    n=$((nr_task * 10))

    
    #判断testcase的值,执行不同的lmbench测试用例
    # 如果 testcase 是 lat_syscall，测试系统调用的延迟  
    # 如果 testcase 是 lat_proc，测试进程创建的延迟  
    # 如果 testcase 是 lat_unix，测试 UNIX 域套接字的延迟  
    # 如果 testcase 是 lat_pipe，测试管道操作的延迟  
    if [ "$testcase" = "lat_syscall" ] || [ "$testcase" = "lat_proc" ] || [ "$testcase" = "lat_unix" ] || [ "$testcase" = "lat_pipe" ]; then
      
       [-n "$systemcall" ] && params="$params $systemcall"
       cd "$TONE_BM_RUN_DIR/bin/$OS"
    #参数：nr_task（任务数）、w（等待时间）、n（迭代次数）、systemcall（可选的系统调用）
       logger ./"$testcase" -P $nr_task -W $w -N $n $systemcall
    # 如果 testcase 是 bw_pipe，测试管道带宽
    # 如果 testcase 是 bw_unix，测试 UNIX 域套接字的带宽    
    elif [ "$testcase" = "bw_pipe" ] || [ "$testcase" = "bw_unix" ]; then
         [-n "$systemcall" ] && params="$params $systemcall"
         cd "$TONE_BM_RUN_DIR/bin/$OS"
         logger ./"$testcase" -m ${KB}KB -M ${MB}MB  -P $nr_task -W $w -N $n  
    # 如果 testcase 是 bw_mem，测试内存带宽  
    elif [ "$testcase" = "bw_mem" ]; then
         cd "$TONE_BM_RUN_DIR/bin/$OS"
         logger ./"$testcase"  -P $nr_task -W $w -N $n  ${MB}MB  $systemcall
    # 如果 testcase 是 lat_mem_rd，测试内存读取延迟  
    elif [ "$testcase" = "lat_mem_rd" ]; then
         [-n "$systemcall" ] && params="$params $systemcall"
         cd "$TONE_BM_RUN_DIR/bin/$OS"
         logger ./"$testcase"  -P $nr_task -W $w -N $n -t ${MB}MB  ${KB}KB
    # 运行 lat_mmap 测试，测量内存映射操作的延迟  
    elif [ "$testcase" = "lat_mmap" ]; then
         dd if=/dev/zero of=/root/$systemcall bs=1M count=1024
         cd "$TONE_BM_RUN_DIR/bin/$OS"
         logger ./"$testcase" -r -C -P $nr_task -W $w -N $n ${MB}K /root/$systemcall
   elif [ "$testcase" = "lmbench" ]; then  
        [ -n "$systemcall" ] && params="$params $systemcall"  
        [ -n "$nr_task" ] && params="$params $nr_task"  
        [ -n "$testcase" ] && params="$params $testcase"  
        cd "$TONE_BM_RUN_DIR" || { echo "无法进入目录 $TONE_BM_RUN_DIR"; exit 1; }  
        sudo chmod +x $TONE_BM_SUITE_DIR/lmbench.exp  
        # 接下来应该调用 lmbench.exp 脚本  
        cd $TONE_BM_SUITE_DIR
        expect lmbench.exp
fi
}

parse() {
    $TONE_BM_SUITE_DIR/parse.awk
}
